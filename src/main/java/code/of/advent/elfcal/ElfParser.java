package code.of.advent.elfcal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class ElfParser {
  
  public List<Elf> parseElves(File inputFile) throws IOException {
    List<Elf> elves = new ArrayList<>();
    try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {
      Elf newElf = new Elf("Elf #0");

      String line = br.readLine();
      while (line != null) {
        if (line.isBlank()) {
          elves.add(newElf);
          newElf = new Elf("Elf #"+elves.size());
        } else {
          Integer calories = Integer.parseInt(line);
          newElf.getItemCals().add(calories);
        }
        line = br.readLine();
      }
    }
    System.out.println("Elf count: " + elves.size());
    return elves;
  }
}
