package code.of.advent.elfcal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElfcalApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElfcalApplication.class, args);
	}

}
