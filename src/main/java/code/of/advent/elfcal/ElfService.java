package code.of.advent.elfcal;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

@Service
public class ElfService {

    @Autowired
    private ElfParser elfParser;

    private List<Elf> elves;

    public List<Elf> getElves() throws IOException{
        if(elves==null){
            File inputFile = ResourceUtils.getFile("classpath:input.txt");
            elves = elfParser.parseElves(inputFile);
        }
        return elves;
    }

    public Elf mostCals() throws IOException{
        return orderElfs().get(elves.size()-1);
    }   

    public List<Elf> orderElfs() throws IOException{
        List<Elf> elves = new ArrayList<>(getElves());
        elves.sort((a, b) -> a.getTotalCals() - b.getTotalCals());
        return elves;
    }
}
