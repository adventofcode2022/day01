package code.of.advent.elfcal;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CalController {

    @Autowired
    private ElfService elfService;

    @GetMapping("elves")
    public List<Elf> getElves() throws IOException {
        return elfService.getElves();

    }

    @GetMapping("mostCals")
    public Elf mostCals() throws IOException {
        return elfService.mostCals();
    }

    @GetMapping("top3")
    public List<Elf> top3Elf() throws IOException {
        List<Elf> elves = elfService.orderElfs();
        return elves.subList(elves.size() -3, elves.size());
    }
}
