package code.of.advent.elfcal;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class Elf {
    private List<Integer> itemCals = new ArrayList<>();

    private final String name;

    public int getTotalCals() {
        int total = 0; 
        for (Integer item : itemCals) {
            total += item;
        }
        return total;
    }
}
